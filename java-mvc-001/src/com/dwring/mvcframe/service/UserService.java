package com.dwring.mvcframe.service;

import java.util.Map;

import com.dwring.mvcframe.model.Message;

public interface UserService {
	Map<String, Object> getMessage(String user);
}
