package com.dwring.mvcframe.service;

import java.util.HashMap;
import java.util.Map;

import com.dwring.mvcframe.model.Message;

public class UserServiceImpl implements UserService {

	@Override
	public Map<String, Object> getMessage(String user) {
		Map<String, Object> map = new HashMap<String, Object>();
		Message message = new Message();
		message.setName("user");
		String url = "";
		int i = 0;
		if (user.equals("admin")) {
			url = "WEB-INF/views/index.jsp";
			message.setMessage("成功！");

		} else {
			message.setMessage("失败！");
			url = "WEB-INF/views/fail.jsp";
		}
		map.put(url, message);
		return map;
	}

	private boolean isUser(String user) {
		boolean b = false;
		if (user.equals("zhanghaichang")) {
			b = true;
		}
		return b;
	}

}
