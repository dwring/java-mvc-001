package com.dwring.mvcframe.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dwring.mvcframe.action.ActionSupport;
import com.dwring.mvcframe.action.UserAction;
import com.dwring.mvcframe.action.form.ActionForm;
import com.dwring.mvcframe.action.form.FullForm;
import com.dwring.mvcframe.model.Message;
import com.dwring.mvcframe.model.XmlBean;

public class ActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ActionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		String name = request.getParameter("name");
		String path = getPath(request.getServletPath());
		Map<String, Object> map = (Map<String, Object>) this
				.getServletContext().getAttribute("struts");

		XmlBean xmlBean = (XmlBean) map.get(path);
		String formClass = xmlBean.getFormClass();
		System.out.println("================xmlBean getName=========== "
				+ xmlBean.getFormClass());
		ActionForm form = FullForm.full(formClass, request);
		String actionType = xmlBean.getActionType();
		ActionSupport action = null;
		String url = "";
		try {
			System.out.println("=========actionType=============" + actionType);
			Class clazz = Class.forName(actionType);
			action = (ActionSupport) clazz.newInstance();
			url = action.execute(request, form, xmlBean.getActionForward());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("严重 exception !");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	private String getPath(String servletPath) {
		return servletPath.split("\\.")[0];
	}

}
