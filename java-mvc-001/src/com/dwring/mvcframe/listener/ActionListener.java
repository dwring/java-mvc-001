package com.dwring.mvcframe.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.dwring.mvcframe.model.XmlBean;
import com.dwring.mvcframe.utils.XmlHelper;

public class ActionListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("系统注销。。。。。");

	}

	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		ServletContext context = contextEvent.getServletContext();
		String xmlPath = context.getInitParameter("struts-config");
		String tomcatPath = contextEvent.getServletContext().getRealPath("\\");
		System.out.println("xml path +tomcat path:"+tomcatPath+xmlPath);
		try {
			Map<String, Object> map = XmlHelper.initXml(tomcatPath + xmlPath);
			context.setAttribute("struts", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("信息系统已经加载完成。");

	}
}
