package com.dwring.mvcframe.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.dwring.mvcframe.action.form.ActionForm;

public interface ActionSupport {
	public String execute(HttpServletRequest request, ActionForm from,
			Map<String, String> map);
}
