package com.dwring.mvcframe.action;

import java.util.Map;


import javax.servlet.http.HttpServletRequest;

import com.dwring.mvcframe.action.form.ActionForm;
import com.dwring.mvcframe.action.form.UserForm;


public class UserAction implements ActionSupport {

	@Override
	public String execute(HttpServletRequest request, ActionForm form,
			Map<String, String> actionForward) {
		String url = "fail";
		UserForm userForm = (UserForm) form;
		if (userForm.getName().equals("admin") &&userForm.getPassword().equals("admin")) {
			url = "success";
		}
		return actionForward.get(url);
	}
}
