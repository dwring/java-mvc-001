package com.dwring.mvcframe.action.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {

	public static ActionForm full(String formPath, HttpServletRequest request) {
		ActionForm form = null;
		try {
			System.out.println("===========formPath============="+formPath);
			Class clazz = Class.forName(formPath);
			form = (ActionForm) clazz.newInstance();
			Field[] field = clazz.getDeclaredFields();
			for (Field f : field) {
				f.setAccessible(true);
				f.set(form, request.getParameter(f.getName()));
				f.setAccessible(false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("类装载失败。。。");
		}
		return form;

	}
}
